from setuptools import setup, Extension


# read the contents of README file
from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
        long_description = f.read()


setup(
    name='jod',
    version='0.0.1',
    author='musingsole',
    author_email='musingsole@gmail.com',
    description='Join, or Die simple asynchronous execution'
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/musingsole/jod',
    install_requires=[],
    packages=["murdaws"],
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
