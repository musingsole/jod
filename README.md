# Join, or Die

![Join, or Die](https://upload.wikimedia.org/wikipedia/commons/9/9c/Benjamin_Franklin_-_Join_or_Die.jpg)

Simple, asynchronous execution of multiple argument sets to one function.
